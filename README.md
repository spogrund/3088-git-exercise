This design project aims to detail a design of a microPi HAT (uHAT) compliant PCB of an
uninterrupted power supply (UPS) “daughter board” for a Raspberry Pi (RPi) Zero single board
computer (SBC). The uHAT is made up of several submodules that enable it to provide power to
a RPi Zero when there is no power being supplied to the RPi by the mains power supply.
The use cases are related to the common practice of setting up a RPi as a home server, network
attached storage (NAS) system, as part of an arrangement with RPi add-ons with mobility for
example a home-surveillance or a dash-cam camera system and general physical computing tasks
with breadboards and software.
The UPS uHAT will attach directly to (below / underneath) the RPi Zero with all RPi input/output
(I/O) access points still available. The UPS uHAT will be affixed and will connect to the RPI via
the bottom of certain general-purpose I/O (GPIO) pins on the RPi. The scenario this UPS uHAT
will be used in is when there is loss of mains power and the RPi Zero device or system must either
continue operating or have enough power to shutdown correctly.

There are three subsystems which include:
	-Power regulation module
	-Power conversion module
	-LED status indication module
Each of the group members has been tasked to design and simulate each of the
circuits.
